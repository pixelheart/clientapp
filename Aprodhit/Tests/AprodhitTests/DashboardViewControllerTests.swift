//
//  DashboardViewControllerTests.swift
//
//
//  Created by Ilham Prabawa on 27/10/23.
//

import XCTest
@testable import AprodhitiOS

final class DashboardViewControllerTests: XCTestCase {

  var sut: DashboardViewController!

  override func setUp() {
    super.setUp()

    sut = DashboardViewController()
  }

  override func tearDown() {
    super.tearDown()

    sut = nil
  }

  func test_loadViewThenDashboardRooViewLoaded() {
    sut.viewDidLoad()

    XCTAssertNotNil(sut.rootView)
  }

  func test_loadViewThenReturnBrownBackground() {

    sut.loadView()

    XCTAssertEqual(sut.rootView.backgroundColor, UIColor.clear)
  }

  func test_loadViewRootViewFrameSameWithViewBounds() {
    sut.loadView()

    XCTAssertEqual(sut.rootView.frame, sut.view.bounds)
  }

  func test_loadViewAndCollectionViewShouldNotNil() {
    sut.loadView()

    XCTAssertNotNil(sut.rootView.collectionView)
  }

  func test_getNumberOfSectionsAndReturnOne() {
    sut.loadView()

    XCTAssertEqual(sut.rootView.collectionView.numberOfSections, 3)
  }

  func test_getCountInSectionOneAndReturnFiveItems() {
    sut.loadView()

    XCTAssertEqual(sut.rootView.collectionView.numberOfItems(inSection: 0), 5)
  }

  func test_getCountInSectionTwoAndReturnOneItem() {
    sut.loadView()

    XCTAssertEqual(sut.rootView.collectionView.numberOfItems(inSection: 1), 1)
  }

  func test_getCountInSectionThreeAndReturnEightItem() {
    sut.loadView()

    XCTAssertEqual(sut.rootView.collectionView.numberOfItems(inSection: 2), 7)
  }

}
