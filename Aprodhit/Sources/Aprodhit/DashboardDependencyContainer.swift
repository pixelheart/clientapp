//
//  DashboardDependencyContainer.swift
//
//
//  Created by Ilham Prabawa on 27/10/23.
//

import Foundation
import AprodhitiOS
import AprodhitKit
import GnDKit

public class DashboardDependencyContainer {

  private let sharedMainViewModel: MainViewModel
  private let dashboardViewModel: DashboardViewModel

  public init(dependencyContainer: MainDependencyContainer) {
    func makeDashboardViewModel() -> DashboardViewModel {
      return DashboardViewModel()
    }

    self.sharedMainViewModel = dependencyContainer.sharedMainViewModel
    self.dashboardViewModel = makeDashboardViewModel()
  }

  public func makeDashboardViewController() -> DashboardViewController {
    let homeFactory = {
      return self.makeHomeViewControllerFactory()
    }

    let listOnlineAdvocateFactory = {
      return self.makeListOnlineAdvocateViewControllerFactory()
    }

    return DashboardViewController(
      viewModel: dashboardViewModel,
      homeViewControllerFactory: homeFactory,
      listOnlineAdvocateViewControllerFactory: listOnlineAdvocateFactory
    )
  }

  //View Controller Factory
  fileprivate func makeHomeViewControllerFactory() -> HomeViewController {
    return HomeViewController(storeFactory: self)
  }

  fileprivate func makeListOnlineAdvocateViewControllerFactory() -> ListOnlineAdvocateViewController {
    return ListOnlineAdvocateViewController()
  }

  //ViewModel Factory
  public func makeHomeViewModel() -> HomeViewModel {
    return HomeViewModel(onlineAdvocateNavigator: dashboardViewModel)
  }

}

extension DashboardDependencyContainer: HomeViewModelFactory {}
