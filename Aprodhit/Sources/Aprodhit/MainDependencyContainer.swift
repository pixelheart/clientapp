//
//  AppDependencyContainer.swift
//
//
//  Created by Ilham Prabawa on 30/09/23.
//

import UIKit
import AprodhitiOS
import AprodhitKit

public class MainDependencyContainer {

  public let sharedMainViewModel: MainViewModel

  public init() {

    func makeMainViewModel() -> MainViewModel {
      return MainViewModel()
    }

    sharedMainViewModel = makeMainViewModel()
  }

  public func makeMainViewController() -> MainTabViewController {

    let dashBoardFactory = {
      return self.makeDashboardViewController()
    }

    let articleFactory = {
      return self.makeArticleViewController()
    }

    let historyFactory = {
      return self.makeHistoryViewController()
    }

    let profileFactory = {
      return self.makeProfileViewController()
    }

    return MainTabViewController(
      sharedMainViewModel: sharedMainViewModel,
      dashboardViewControllerFactory: dashBoardFactory,
      articleViewControllerFactory: articleFactory,
      historyViewControllerFactory: historyFactory,
      profileViewControllerFactory: profileFactory
    )

  }

  public func makeDashboardViewController() -> DashboardViewController {
    let iconImage = UIImage(named: "ic_home_unselected",
                            in: .module,
                            compatibleWith: .none)?
      .withRenderingMode(.alwaysOriginal)

    let selectedImage = UIImage(named: "ic_home_selected",
                                in: .module,
                                compatibleWith: .none)?
      .withRenderingMode(.alwaysOriginal)

    let vc = DashboardDependencyContainer(dependencyContainer: self).makeDashboardViewController()
    vc.tabBarItem.title = "Beranda"
    vc.tabBarItem.image = iconImage
    vc.tabBarItem.selectedImage = selectedImage
    vc.tabBarItem.setTitleTextAttributes(
      [
        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .medium),
        NSAttributedString.Key.foregroundColor: UIColor.buttonActiveColor
      ],
      for: .normal
    )
    return vc
  }

  public func makeProfileViewController() -> ProfileViewController {
    let iconImage = UIImage(named: "profile_unselected",
                            in: .module,
                            compatibleWith: .none)?
      .withRenderingMode(.alwaysOriginal)

    let selectedImage = UIImage(named: "profile_selected",
                                in: .module,
                                compatibleWith: .none)?
      .withRenderingMode(.alwaysOriginal)

    let vc = ProfileViewController()
    vc.tabBarItem.title = "Akun"
    vc.tabBarItem.image = iconImage
    vc.tabBarItem.selectedImage = selectedImage
    vc.tabBarItem.setTitleTextAttributes(
      [
        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .medium),
        NSAttributedString.Key.foregroundColor: UIColor.buttonActiveColor
      ],
      for: .normal
    )
    return vc
  }

  public func makeArticleViewController() -> ArticleViewController {
    let iconImage = UIImage(named: "article_unselected",
                            in: .module,
                            compatibleWith: .none)?
      .withRenderingMode(.alwaysOriginal)

    let selectedImage = UIImage(named: "article_selected",
                                in: .module,
                                compatibleWith: .none)?
      .withRenderingMode(.alwaysOriginal)

    let vc = ArticleViewController()
    vc.tabBarItem.title = "Artikel"
    vc.tabBarItem.image = iconImage
    vc.tabBarItem.selectedImage = selectedImage
    vc.tabBarItem.setTitleTextAttributes(
      [
        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .medium),
        NSAttributedString.Key.foregroundColor: UIColor.buttonActiveColor
      ],
      for: .normal
    )
    return vc
  }

  public func makeHistoryViewController() -> HistoryViewController {
    let iconImage = UIImage(named: "history_unselected",
                            in: .module,
                            compatibleWith: .none)?
      .withRenderingMode(.alwaysOriginal)

    let selectedImage = UIImage(named: "history_selected",
                                in: .module,
                                compatibleWith: .none)?
      .withRenderingMode(.alwaysOriginal)

    let vc = HistoryViewController()
    vc.tabBarItem.title = "Riwayat"
    vc.tabBarItem.image = iconImage
    vc.tabBarItem.selectedImage = selectedImage
    vc.tabBarItem.setTitleTextAttributes(
      [
        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .medium),
        NSAttributedString.Key.foregroundColor: UIColor.buttonActiveColor
      ],
      for: .normal
    )
    return vc
  }

}
