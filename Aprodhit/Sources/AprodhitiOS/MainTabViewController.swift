//
//  MainTabViewController.swift
//
//
//  Created by Ilham Prabawa on 26/10/23.
//

import Foundation
import UIKit
import GnDKit
import AprodhitKit
import Combine

public class MainTabViewController: UITabBarController {

  //Dependency
  public let sharedMainViewModel: MainViewModel

  //Factory Closure
  private let makeDashboardViewController: () -> UIViewController
  private let makeArticleViewController: () -> UIViewController
  private let makeHistoryViewController: () -> UIViewController
  private let makeProfileViewController: () -> UIViewController

  private var subscriptions = Set<AnyCancellable>()

  public init(
    sharedMainViewModel: MainViewModel,
    dashboardViewControllerFactory: @escaping () -> UIViewController,
    articleViewControllerFactory: @escaping () -> UIViewController,
    historyViewControllerFactory: @escaping () -> UIViewController,
    profileViewControllerFactory: @escaping () -> UIViewController
  ) {

    self.sharedMainViewModel = sharedMainViewModel
    self.makeDashboardViewController = dashboardViewControllerFactory
    self.makeArticleViewController = articleViewControllerFactory
    self.makeHistoryViewController = historyViewControllerFactory
    self.makeProfileViewController = profileViewControllerFactory

    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  public override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = .white
    tabBar.isTranslucent = true


    buildTab()
  }

  fileprivate func buildTab(){
    viewControllers = [
      makeDashboardViewController(),
      makeArticleViewController(),
      makeHistoryViewController(),
      makeProfileViewController()
    ]
  }

  fileprivate func present(_ view: MainView) {
    switch view {
    case .main(let tab):
      self.selectedIndex = tab.rawValue
    case .appVersion:
      break
    }
  }

  fileprivate func subscribe(_ publisher: AnyPublisher<MainView, Never>) {
    publisher.removeDuplicates()
      .receive(on: RunLoop.main)
      .subscribe(on: DispatchQueue.main)
      .sink { [weak self] view in
        self?.present(view)
      }.store(in: &subscriptions)
  }

  fileprivate func observeViewModel() {
    let publisher = sharedMainViewModel.$mainView.eraseToAnyPublisher()
    subscribe(publisher)

  }

}
