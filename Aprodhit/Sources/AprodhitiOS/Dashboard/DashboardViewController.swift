//
//  DashboardViewController.swift
//
//
//  Created by Ilham Prabawa on 27/10/23.
//

import UIKit
import SwiftUI
import AprodhitKit
import GnDKit
import Combine

public class DashboardViewController: NiblessNavigationController {

  private let viewModel: DashboardViewModel

  private let makeHomeViewController: () -> HomeViewController
  private let makeListOnlineAdvocateViewController: () -> ListOnlineAdvocateViewController

  private var subscriptions = Set<AnyCancellable>()

  public init(
    viewModel: DashboardViewModel,
    homeViewControllerFactory: @escaping () -> HomeViewController,
    listOnlineAdvocateViewControllerFactory: @escaping () -> ListOnlineAdvocateViewController
  ) {

    self.viewModel = viewModel
    self.makeHomeViewController = homeViewControllerFactory
    self.makeListOnlineAdvocateViewController = listOnlineAdvocateViewControllerFactory

    super.init()

    delegate = self
    
    observeViewModel()
  }

  fileprivate func subscribe(_ publisher: AnyPublisher<DashboardNavigationAction, Never>) {
    publisher.removeDuplicates()
      .receive(on: RunLoop.current)
      .subscribe(on: DispatchQueue.main)
      .sink { [weak self] action in
        self?.respond(to: action)
      }.store(in: &subscriptions)
  }

  fileprivate func respond(to navigationAction: DashboardNavigationAction) {
    switch navigationAction {
    case .present(let view):
      present(view: view)
    case .presented:
      break
    }
  }

  fileprivate func present(view: DashboardView) {
    switch view {
    case .home:
      presentHome()
    case .onlineAdvocate:
      presentOnlineAdvocate()
    }
  }

  fileprivate func presentHome() {
    pushViewController(makeHomeViewController(), animated: true)
  }

  fileprivate func presentOnlineAdvocate() {
    pushViewController(makeListOnlineAdvocateViewController(), animated: true)
  }

  fileprivate func observeViewModel() {
    subscribe(viewModel.$navigationAction.eraseToAnyPublisher())
  }

}

// MARK: - Navigation Bar Presentation
extension DashboardViewController {

  func hideOrShowNavigationBarIfNeeded(for view: DashboardView, animated: Bool) {
    if view.hidesNavigationBar() {
      hideNavigationBar(animated: animated)
    } else {
      showNavigationBar(animated: animated)
    }
  }

  func hideNavigationBar(animated: Bool) {
    if animated {
      transitionCoordinator?.animate(alongsideTransition: { context in
        self.setNavigationBarHidden(true, animated: animated)
      })
    } else {
      setNavigationBarHidden(true, animated: false)
    }
  }

  func showNavigationBar(animated: Bool) {
    if self.isNavigationBarHidden {
      self.setNavigationBarHidden(false, animated: animated)
    }
  }

}

// MARK: - UINavigationControllerDelegate
extension DashboardViewController: UINavigationControllerDelegate {

  public func navigationController(
    _ navigationController: UINavigationController,
    willShow viewController: UIViewController,
    animated: Bool
  ) {
    guard let viewToBeShown = dashboardView(associatedWith: viewController) else { return }
    hideOrShowNavigationBarIfNeeded(for: viewToBeShown, animated: animated)
  }

  public func navigationController(
    _ navigationController: UINavigationController,
    didShow viewController: UIViewController,
    animated: Bool
  ) {
    guard let shownView = dashboardView(associatedWith: viewController) else { return }
    viewModel.uiPresented(dashboardView: shownView)
  }

}

extension DashboardViewController {

  func dashboardView(associatedWith viewController: UIViewController) -> DashboardView? {
    switch viewController {
    case is HomeViewController:
      return .home
    case is ListOnlineAdvocateViewController:
      return .onlineAdvocate
    default:
      assertionFailure("Encountered unexpected child view controller type in dashboardviewcontroller")
      return nil
    }
  }

}


