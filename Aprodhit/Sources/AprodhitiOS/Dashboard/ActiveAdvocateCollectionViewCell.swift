//
//  ActiveAdvocateCollectionViewCell.swift
//
//
//  Created by Ilham Prabawa on 27/10/23.
//

import UIKit

public class ActiveAdvocateCollectionViewCell: UICollectionViewCell {
  
  public static let reuseIdentifier = "ActiveAdvocateCollectionViewCell"

  public override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = .red
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
