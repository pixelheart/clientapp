//
//  DashboardUIView.swift
//
//
//  Created by Ilham Prabawa on 28/10/23.
//

import SwiftUI
import GnDKit
import AprodhitKit

struct HomeUIView: View {
  
  @ObservedObject var store: HomeViewModel
  
  @State var offSet: CGFloat = 0
  
  let maxHeight = screen.height < 750
  ? (screen.height / 1.9)
  : (screen.height / 2.3)
  
  var body: some View {
    ZStack {
      
      ScrollView {
        
        headerView()
        
        VStack {
          activeConsultation()
          
          seeAllAdvocate()
          
          gridCategory()
        }
        
      }
      .coordinateSpace(name: "SCROLL")
    }
    .edgesIgnoringSafeArea(.all)
  }
  
  @ViewBuilder
  func headerView() -> some View {
    VStack {
      GeometryReader { geo in
        VStack {
          VStack {
            TextField("Cari Advokate", text: .constant(""))
              .padding(.horizontal, 8)
            
          }
          .frame(maxWidth: .infinity, minHeight: 50)
          .background(Color.white)
        }
        .frame(height: getHeaderHeight(), alignment: .center)
        .padding(.top, 24)
        .padding(.horizontal, 16)
        .background(Color.blue)
        
      }
      .frame(height: 200)
      .offset(y: -offSet)
    }
    .zIndex(offSet < -80 ? 1 : 0)
    .modifier(OffsetModifier(offset: $offSet))
    
  }
  
  @ViewBuilder
  func activeConsultation() -> some View {
    ScrollView(.horizontal, showsIndicators: false) {
      LazyHStack {
        ForEach(0..<10) { i in
          VStack {
            
          }
          .frame(width: 230, height: 160)
          .background(Color.red)
        }
      }
      .padding(.horizontal, 16)
    }
  }
  
  @ViewBuilder
  func seeAllAdvocate() -> some View {
    Button {
      store.navigateToSeeAll()
    } label: {
      Text("Semua Advokat Online")
        .foregroundColor(Color.white)
        .frame(maxWidth: .infinity, minHeight: 60)
        .background(Color.purple)
        .padding(.horizontal, 16)
    }
  }
  
  @ViewBuilder
  func gridCategory() -> some View {
    LazyVGrid(
      columns: [
        GridItem(.fixed(screen.width / 2)),
        GridItem(.fixed(screen.width / 2))
      ],
      content: {
        ForEach(0..<23) { i in
          VStack {
            
          }
          .frame(maxWidth: .infinity, minHeight: 60)
          .background(Color.yellow)
          .padding(.leading, 16)
          .padding(.trailing, 16)
          
        }
      }
      
    )
  }
  
  func getHeaderHeight() -> CGFloat{
    let topHeight = 250 + offSet
    return topHeight > 130 ? topHeight : 130
  }
  
}

#Preview {
  HomeUIView(
    store: HomeViewModel(
      onlineAdvocateNavigator: DashboardViewModel()
    )
  )
}

struct OffsetModifier: ViewModifier {
  
  @Binding var offset: CGFloat
  
  func body(content: Content) -> some View {
    content
      .overlay(
        GeometryReader { proxy -> Color in
          let minY = proxy.frame(in: .named("SCROLL")).minY
          
          DispatchQueue.main.async {
            self.offset = minY
          }
          
          return Color.clear
        },
        alignment: .top
      )
  }
  
}
