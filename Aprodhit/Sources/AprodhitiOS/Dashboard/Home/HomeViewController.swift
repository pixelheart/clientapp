//
//  HomeViewController.swift
//
//
//  Created by Ilham Prabawa on 28/10/23.
//

import UIKit
import SwiftUI
import GnDKit
import AprodhitKit

public class HomeViewController: NiblessViewController {

  private let storeFactory: HomeViewModelFactory

  public init(storeFactory: HomeViewModelFactory) {
    self.storeFactory = storeFactory
    super.init()
  }

  public override func loadView() {
    super.loadView()

    let contentView = UIHostingController(
      rootView: HomeUIView(store: storeFactory.makeHomeViewModel())
    )
    addFullScreen(childViewController: contentView)
  }

  public override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = .clear
  }
}
