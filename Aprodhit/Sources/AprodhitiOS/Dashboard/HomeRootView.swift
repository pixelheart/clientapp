//
//  HomeRootView.swift
//
//
//  Created by Ilham Prabawa on 27/10/23.
//

import UIKit
import GnDKit

public class HomeRootView: NiblessView {

  private enum DashboardSection: Int {
    case activeAdvocate
    case category
  }

  //Properties
  var didKeyboardClosed: Bool = false
  var headerHeightConstraint: NSLayoutConstraint!

  //View
  lazy var collectionView: UICollectionView = {
    let cv = UICollectionView(frame: .zero, collectionViewLayout: createLayout())
    cv.showsVerticalScrollIndicator = false
    cv.register(
      ActiveAdvocateCollectionViewCell.self,
      forCellWithReuseIdentifier: ActiveAdvocateCollectionViewCell.reuseIdentifier
    )
    cv.translatesAutoresizingMaskIntoConstraints = false
    cv.dataSource = self
    return cv
  }()

  let searchTextField: UITextField = {
    let tf = UITextField()
    tf.placeholder = "Cari Advokat"
    tf.backgroundColor = .white
    tf.translatesAutoresizingMaskIntoConstraints = false
    return tf
  }()

  lazy var headerView: UIView = {
    let view = UIView(frame: .zero)
    view.translatesAutoresizingMaskIntoConstraints = false
    view.backgroundColor = .yellow
    return view
  }()

  public init() {
    super.init(frame: .zero)
    backgroundColor = .clear
    setupView()
  }

  fileprivate func setupView() {
    self.addSubview(headerView)
    self.headerView.addSubview(searchTextField)
    self.addSubview(collectionView)

    NSLayoutConstraint.activate([
      headerView.topAnchor.constraint(equalTo: topAnchor),
      headerView.leadingAnchor.constraint(equalTo: leadingAnchor),
      headerView.trailingAnchor.constraint(equalTo: trailingAnchor),
      headerView.heightAnchor.constraint(equalToConstant: 200)
    ])

    NSLayoutConstraint.activate([
      searchTextField.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
      searchTextField.leadingAnchor.constraint(equalTo: headerView.leadingAnchor),
      searchTextField.trailingAnchor.constraint(equalTo: headerView.trailingAnchor),
      searchTextField.heightAnchor.constraint(equalToConstant: 40)
    ])

    NSLayoutConstraint.activate([
      collectionView.topAnchor.constraint(equalTo: headerView.bottomAnchor),
      collectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
      collectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
      collectionView.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor)
    ])

  }

}

extension HomeRootView: UICollectionViewDataSource {

  public func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 3
  }

  public func collectionView(
    _ collectionView: UICollectionView,
    numberOfItemsInSection section: Int
  ) -> Int {
    switch section {
    case 0:
      return 5
    case 1:
      return 1
    case 2:
      return 7
    default:
      return 0
    }
  }

  public func collectionView(
    _ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath
  ) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(
      withReuseIdentifier: ActiveAdvocateCollectionViewCell.reuseIdentifier,
      for: indexPath
    )
    return cell
  }

}

extension HomeRootView {

  func createLayout() -> UICollectionViewLayout {
    let layout = UICollectionViewCompositionalLayout { (section, env) -> NSCollectionLayoutSection in
      switch section {
      case 0:
        return self.activeLayoutSection()
      case 1:
        return self.buttonLayoutSection()
      default:
        return self.categoryLayoutSection()
      }
    }

    return layout
  }

  fileprivate func activeLayoutSection() -> NSCollectionLayoutSection {
    let itemSize = NSCollectionLayoutSize(
      widthDimension: .fractionalWidth(0.5),
      heightDimension: .fractionalHeight(1.0)
    )

    let item = NSCollectionLayoutItem(layoutSize: itemSize)
    item.contentInsets = NSDirectionalEdgeInsets(
      top: 0,
      leading: 4,
      bottom: 0,
      trailing: 4
    )

    let groupSize = NSCollectionLayoutSize(
      widthDimension: .fractionalWidth(1.0),
      heightDimension: .absolute(100)
    )

    let group = NSCollectionLayoutGroup.horizontal(
      layoutSize: groupSize,
      subitems: [item]
    )

    let section = NSCollectionLayoutSection(group: group)
    section.orthogonalScrollingBehavior = .continuous
    section.contentInsets = NSDirectionalEdgeInsets(
      top: 8,
      leading: 4,
      bottom: 8,
      trailing: 4
    )

    return section
  }

  fileprivate func buttonLayoutSection() -> NSCollectionLayoutSection {
    let itemSize = NSCollectionLayoutSize(
      widthDimension: .fractionalWidth(1.0),
      heightDimension: .fractionalHeight(1.0)
    )

    let item = NSCollectionLayoutItem(layoutSize: itemSize)
    item.contentInsets = .init(top: 0, leading: 8, bottom: 0, trailing: 8)

    let groupSize = NSCollectionLayoutSize(
      widthDimension: .fractionalWidth(1.0),
      heightDimension: .absolute(48)
    )

    let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])

    let section = NSCollectionLayoutSection(group: group)

    return section
  }

  fileprivate func categoryLayoutSection() -> NSCollectionLayoutSection {
    let itemSize = NSCollectionLayoutSize(
      widthDimension: .fractionalWidth(0.5),
      heightDimension: .fractionalHeight(1.0)
    )

    let item = NSCollectionLayoutItem(layoutSize: itemSize)
    item.contentInsets = NSDirectionalEdgeInsets(
      top: 0,
      leading: 0,
      bottom: 5,
      trailing: 0
    )

    let groupSize = NSCollectionLayoutSize(
      widthDimension: .fractionalWidth(1.0),
      heightDimension: .fractionalWidth(0.2)
    )

    let group = NSCollectionLayoutGroup.horizontal(
      layoutSize: groupSize,
      subitems: [item]
    )

    group.interItemSpacing = .fixed(8)

    let section = NSCollectionLayoutSection(group: group)
    section.contentInsets = NSDirectionalEdgeInsets(
      top: 8,
      leading: 8,
      bottom: 0,
      trailing: 8
    )

    return section

  }

}

extension HomeRootView: UISearchBarDelegate {

  public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    didKeyboardClosed = false
    searchBar.becomeFirstResponder()
  }

  public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    didKeyboardClosed = false
    searchBar.resignFirstResponder()
  }

  public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

    if searchText.isEmpty {

    }

    if searchText.count > 2 {

    }
  }

}
