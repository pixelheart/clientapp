//
//  DashboardResponder.swift
//
//
//  Created by Ilham Prabawa on 26/10/23.
//

import Foundation

public protocol DashboardResponder {
  func gotoDashboard()
}
