//
//  SeeAllNavigator.swift
//
//
//  Created by Ilham Prabawa on 28/10/23.
//

import Foundation

public protocol OnlineAdvocateNavigator {
  func navigateToListAdvocate()
}
