//
//  LoginResponder.swift
//
//
//  Created by Ilham Prabawa on 26/10/23.
//

import Foundation

public protocol LoginResponder {
  func gotoToLogin()
}
