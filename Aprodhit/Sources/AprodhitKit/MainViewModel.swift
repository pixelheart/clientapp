//
//  File.swift
//
//
//  Created by Ilham Prabawa on 26/10/23.
//

import Foundation

public class MainViewModel: LoginResponder,
                            DashboardResponder {

  @Published public private(set) var mainView: MainView = .main(.dashboard)

  public init() {}

  public func gotoToLogin() {
    mainView = .main(.profile)
  }

  public func gotoDashboard() {
    mainView = .main(.dashboard)
  }

}
