//
//  MainView.swift
//
//
//  Created by Ilham Prabawa on 26/10/23.
//

import Foundation

public enum Tab: Int{
  case dashboard = 0
  case article = 1
  case history = 2
  case profile = 3
}

public enum MainView{
  case main(Tab)
  case appVersion
}

extension MainView: Equatable {
  public static func ==(lhs: MainView, rhs: MainView) -> Bool {
    switch (lhs, rhs) {
    case let (.main(l), .main(r)):
      return l == r
    case (.appVersion, appVersion):
      return true
    case (.main, _),
      (.appVersion, _):
      return false
    }
  }
}

