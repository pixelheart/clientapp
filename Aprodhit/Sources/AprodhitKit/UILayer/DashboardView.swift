//
//  DashboardView.swift
//
//
//  Created by Ilham Prabawa on 28/10/23.
//

import Foundation

public enum DashboardView {
  case home
  case onlineAdvocate

  public func hidesNavigationBar() -> Bool {
    switch self {
    case .home:
      return true
    case .onlineAdvocate:
      return false
    }
  }

}
