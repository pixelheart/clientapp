//
//  DashboardViewModel.swift
//
//
//  Created by Ilham Prabawa on 28/10/23.
//

import Foundation
import GnDKit

public typealias DashboardNavigationAction = NavigationAction<DashboardView>

public class DashboardViewModel: OnlineAdvocateNavigator {

  @Published public private(set) var navigationAction: DashboardNavigationAction = .present(view: .home)

  public init() {}

  public func navigateToListAdvocate() {
    navigationAction = .present(view: .onlineAdvocate)
  }

  public func uiPresented(dashboardView: DashboardView) {
    navigationAction = .presented(view: dashboardView)
    GLog("presented", dashboardView)
  }

}
