//
//  HomeViewModel.swift
//  
//
//  Created by Ilham Prabawa on 30/09/23.
//

import Foundation

public class HomeViewModel: ObservableObject {
  
  private let onlineAdvocateNavigator: OnlineAdvocateNavigator

  public init(onlineAdvocateNavigator: OnlineAdvocateNavigator) {
    self.onlineAdvocateNavigator = onlineAdvocateNavigator
  }

  public func navigateToSeeAll() {
    onlineAdvocateNavigator.navigateToListAdvocate()
  }

}

public protocol HomeViewModelFactory {

  func makeHomeViewModel() -> HomeViewModel

}
