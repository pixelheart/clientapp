// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
  name: "Aprodhit",
  platforms: [.iOS(.v14)],
  products: [
    // Products define the executables and libraries a package produces, and make them visible to other packages.
    .library(
      name: "Aprodhit",
      targets: ["Aprodhit"]
    ),
    .library(
      name: "AprodhitiOS",
      targets: ["AprodhitiOS"]
    ),
    .library(
      name: "AprodhitKit",
      targets: ["AprodhitKit"]
    ),
  ],
  dependencies: [
    // Dependencies declare other packages that this package depends on.
    .package(
      url: "https://gitlab.com/pixelheart/gndkit.git",
      branch: "develop"
    ),
    .package(
      url: "https://github.com/JonasGessner/JGProgressHUD.git",
      exact: "2.2.0"
    ),
  ],
  targets: [
    // Targets are the basic building blocks of a package. A target can define a module or a test suite.
    // Targets can depend on other targets in this package, and on products in packages this package depends on.
    .target(
      name: "Aprodhit",
      dependencies: [
        "AprodhitiOS",
        "JGProgressHUD",
        .product(name: "GnDKit", package: "gndkit"),
      ],
      resources: [
        .process("Resources")
      ]
    ),
    .target(
      name: "AprodhitiOS",
      dependencies: [
        "AprodhitKit",
        "JGProgressHUD",
        .product(name: "GnDKit", package: "gndkit")
      ],
      resources: [
        .process("Resources")
      ]
    ),
    .target(
      name: "AprodhitKit",
      dependencies: [
        "JGProgressHUD",
        .product(name: "GnDKit", package: "gndkit"),
      ]
    ),
    .testTarget(
      name: "AprodhitTests",
      dependencies: ["Aprodhit"]),
  ]
)
